--------------------------------------------------------------------------------
-- SSRAM
-- Dr THIEBOLT Francois
--------------------------------------------------------------------------------

------------------------------------------------------------------
-- RAM Statique Synchrone - mode burst -
-- Les donnes sur DBUS changent d'etat juste apres le front
-- 	montant CLK. La memoire n'est pas circulaire, c.a.d que lorsque
--		l'adresse depasse la capacite, DBUS <= Z
-- Elle dispose d'un parametre fixant la latence au chip
-- select, c.a.d que lorsque CS* est actif, il se passe CS_LATENCY
-- cycles avant que l'operation READ ou WRITE se fasse effectivement
-- Une operation READ ou WRITE dure tant que CS est actif.
-- L'adresse de l'operation est echantillonnee sur front descendant
--		de CS*, puis incrementee tacitement apres CS_LATENCY cycles
--		et ce tant que CS* est actif.
------------------------------------------------------------------

-- Definition des librairies
library IEEE;

-- Definition des portee d'utilisation
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

-- Definition de l'entite
entity ssram is

	generic (
		-- taille du bus d'adresse
		ABUS_WIDTH : natural := 4; -- soit 16 mots memoire

		-- taille du bus donnee
		DBUS_WIDTH : natural := 8;
		
		-- chip select latence en nombre de cycles
		CS_LATENCY : natural := 4;
		
		-- delai de propagation entre la nouvelle adresse et la donnee sur la sortie
		I2Q : time := 2 ns );

	port (
		-- signaux de controle
		RW		: in std_logic; -- R/W* (W actif a l'etat bas)
		CS,RST	: in std_logic; -- actifs a l'etat bas
		CLK		: in std_logic;

		-- bus d'adresse et de donnee
		ABUS : in std_logic_vector(ABUS_WIDTH-1 downto 0);
		DBUS : inout std_logic_vector(DBUS_WIDTH-1 downto 0) );

end ssram;

-- -----------------------------------------------------------------------------
-- Definition de l'architecture de la ssram
-- -----------------------------------------------------------------------------
architecture behavior of ssram is

	-- definition de constantes

	-- definitions de types (index type default is integer)
	type FILE_REG_typ is array (0 to 2**ABUS_WIDTH-1) of std_logic_vector (DBUS_WIDTH-1 downto 0);

	-- definition des ressources internes
	signal REGS : FILE_REG_typ; -- le banc de registres
	signal LADR: std_logic_vector(ABUS_WIDTH-1 downto 0);
	signal CSOK: std_logic;
begin

DBUS <= REGS(conv_integer(LADR)) when CSOK = '1' and RW = '1' else (others => 'Z');

P_ADR:process(RST,CLK,CS)
	variable latency : natural range 0 to 31 ;
begin 
	if RST ='0' or CS='1' then 
		latency := CS_LATENCY-1;
		CSOK <= '0';
	elsif falling_edge(CS) then 
		LADR <= ABUS;
	elsif rising_edge(CLK) then 
		if latency = 0 then 
			if LADR = 2**ABUS_WIDTH - 1 then
				CSOK <= '0';
			else 
				LADR <= LADR + '1';
			end if;
		else 
			latency := latency -1;
			if latency = 0 then 
				CSOK <= '1';
			end if;
		end if;
	end if;
end process P_ADR;

P_WRITE:process(CLK)
begin 
	if rising_edge(CLK) and RST = '1' and CSOK = '1' and RW = '0' then 
		REGS(conv_integer(LADR)) <= DBUS;
	end if;
end process P_WRITE;

end behavior;
